const express = require('express')
const app = express()
const port = 3002
var cors = require('cors')

const fs = require('fs')

app.use(cors())
app.use(express.json())

const filePath = './TODO.json'
let tasksData = []

const writeToFile = data => {
  fs.writeFile(filePath, JSON.stringify(data, null, 2), err => {
    if (err) throw err
  })
}

if (fs.existsSync(filePath)) {
  tasksData = JSON.parse(fs.readFileSync(filePath))
} else {
  writeToFile([])
}

app.get('/tasks', (req, res) => {
  res.json(tasksData)
})

app.post('/tasks', (req, res) => {
  tasksData.push(req.body)

  writeToFile(tasksData)
  res.json(tasksData[tasksData.length - 1])
})

app.delete('/tasks', (req, res) => {
  const taskId = req.body.id
  tasksData = tasksData.filter(task => taskId !== task.id)
  writeToFile(tasksData)
  res.send(tasksData)
})

app.put('/tasks', (req, res) => {
  const id = req.body.id
  const newTaskName = req.body.newTitle

  const newTasks = []

  if (newTaskName !== '') {
    const index = tasksData.findIndex(task => task.id === id)
    const newTasks = [...tasksData]
    newTasks.splice(index, 1, {
      ...tasksData[index],
      title: newTaskName
    })

    tasksData = [...newTasks]
    writeToFile(tasksData)
  } else if (id) {
    for (const task of tasksData) {
      newTasks.push({
        ...task,
        done: task.id === id ? !task.done : task.done
      })
    }
    tasksData = [...newTasks]
    writeToFile(tasksData)
  } else {
    console.log('err id undefined')
  }
  res.json(tasksData)
})

app.listen(port, () => {
  console.log(`CORS-enabled web server listening on port ${port}`)
})
